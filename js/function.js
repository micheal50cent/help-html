

(function($) {

 	"use strict";
  	$(document).ready(function() {
		
		//progress bar js
         function animateElements() {
             $('.progressbar').each(function () {
                 var elementPos = $(this).offset().top;
                 var topOfWindow = $(window).scrollTop();
                 var percent = $(this).find('.circle').attr('data-percent');
                 var percentage = parseInt(percent, 10) / parseInt(100, 10);
                 var animate = $(this).data('animate');
                 if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
                     $(this).data('animate', true);
                     $(this).find('.circle').circleProgress({
                         startAngle: -Math.PI / 2,
                         value: percent / 100,
                         thickness: 5,
                         fill: {
                             color: '#fecb19'
                         },
                         emptyFill: '#f0f0f0',
                         animation: {
                             duration: 2000,
                             easing: 'easeOutBounce' // Default circleProgressEasing . You can also use jquery Easeing another Effect
                         },
                     }).on('circle-animation-progress', function (event, progress, stepValue) {
                         $(this).find('div').text((stepValue * 100).toFixed(0) + "%");
                     }).stop();
                 }
             })
         };
         // Show animated elements
         animateElements();
         $(window).scroll(animateElements);

        //  progress bar js
		
		 //menu top fixed
        var fixed_top = $(".main-menu");
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 80) {
                fixed_top.addClass("menu-fixed animated fadeInDown");
            } else {
                fixed_top.removeClass("menu-fixed animated fadeInDown");
            }

        });

        //Js code for search box
        $(".first-click").on("click", function () {
            $(".first-click").hide();
            $(".second-click").css('display', 'block');
            $(".main-menu").addClass("search-box");
        });
        $(".second-click").on("click", function () {
            $(".second-click").hide();
            $(".first-click").css('display', 'block');
            $(".main-menu").removeClass("search-box");
        });
        
        


	});

})(jQuery);
 